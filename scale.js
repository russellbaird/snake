class Scale {
    constructor (context, x, y) {
        this.currentPosition = new Position(context, x,y)
        this.previousPosition = undefined // this.curentPosition.copy()
        this.follower = undefined // new Scale(context, x,y)
    }

    moveUp() {
        console.log('write the code to moveUp')
    }

    moveDown() {
        console.log('write the code to moveDown')
    }

    moveLeft() {
        console.log('write the code to moveLeft')
    }

    moveRight() {
        console.log('write the code to moveRight')
    }

    grow() {
        console.log('write the code to add one more scale to the snake')
    }

    contains(location) {
        console.log(`returning true iff this contains ${location.x}, ${location.y}`)
        return this.CurrentPosition.contains(location) || this.follower && this.follower.contains(location)
    }
    move() {
        console.log (`moving ${document.location}`)
        switch (document.direction) {
            // these should match the variables in snake.js
        case 0:
            this.moveUp()
            break
        case 1:
            this.moveLeft()
            break
        case 2:
            this.moveRight()
            break
        case 3:
            this.moveDown()
            break
        }
    }
}
