class Position {
    constructor(context, x,y) {
        this.x = x
        this.y = y
        this.size = 10
        this.context = context
        console.log(`just constructed a position at ${x}, ${y} of size ${this.size}`)
    }

    draw() {
        // code here to draw a rect at this.x,thix.y with sides of length this.size
        console.log(`just drew a position at ${this.x}, ${this.y}`)
    }

    erase() {
        // code here to erase a rect at this.x, this.y with sides of length this.size
        console.log(`just erased a position at ${this.x}, ${this.y}`)
    }

    getLocation() {
        console.log(`returning a position at location ${this.x}, ${this.y}`)
        return {x: this.x, y: this.y}
    }

    setLocation(location) {
        this.x = location.x
        this.y = location.y
        console.log(`just set a position at location ${this.x}, ${this.y}`)
    }

    copy() {
        console.log(`returning a copy of a position at location ${this.x}, ${this.y}`)
        return new Position(this.context, this.x, this.y)
    }

    contains(location) {
        console.log(`testing to see if ${location.x}, ${location.y} is contained in this position: ${this.x}, ${this.y}`)
        return this.x === location.x && this.y === location.y
    }
    // other methods go here
}
