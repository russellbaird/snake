function startSnake() {
    console.log('starting the snake')
    document.canvas = document.getElementById("theSnakeCanvas")
    document.context = document.canvas.getContext('2d')
    document.mealX = 60 // generate a random number here
    document.mealY = 60 // generate a random number here
    document.x = 30 // generate a random number here
    document.y = 30 // generate a random number here
    document.snake = new Scale(document.context, document.x, document.y)
    document.meal = new Meal(document.context, document.mealX, document.mealY)
    document.UP = 0
    document.DOWN = 1
    document.LEFT = 2
    document.RIGHT = 3
    document.direction = document.LEFT

    setInterval(moveSnake, 1000) // 10000 milliseconds
}

function moveSnake()  {
    document.snake.move()
}

document.onkeydown = function(e) {
    switch (e.keyCode) {
        case 37:
            document.direction = document.DOWN
            break;
        case 38:
            document.direction = document.UP
            break;
        case 39:
            document.direction = document.LEFT
            break;
        case 40:
            document.direction = document.RIGHT
            break;
    }
};
